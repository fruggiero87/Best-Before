package de.beocode.bestbefore.data

import android.content.Context
import androidx.datastore.core.CorruptionException
import androidx.datastore.core.DataStore
import androidx.datastore.core.Serializer
import androidx.datastore.core.handlers.ReplaceFileCorruptionHandler
import androidx.datastore.dataStore
import com.google.protobuf.InvalidProtocolBufferException
import de.beocode.bestbefore.Data
import java.io.InputStream
import java.io.OutputStream

object DataSerializer : Serializer<Data> {
    override val defaultValue: Data
        get() = Data.getDefaultInstance().toBuilder().setWarningDays(3).build()
    @Suppress("BlockingMethodInNonBlockingContext")
    override suspend fun readFrom(input: InputStream): Data {
        try {
            return Data.parseFrom(input)
        } catch (e: InvalidProtocolBufferException) {
            throw CorruptionException("Cannot read proto.", e)
        }
    }
    @Suppress("BlockingMethodInNonBlockingContext")
    override suspend fun writeTo(t: Data, output: OutputStream) = t.writeTo(output)
}
val Context.dataDataStore: DataStore<Data> by dataStore(
    fileName = "data.pb",
    serializer = DataSerializer,
    corruptionHandler = ReplaceFileCorruptionHandler(
        produceNewData = { Data.getDefaultInstance().toBuilder().setWarningDays(3).build() }
    )
)