package de.beocode.bestbefore

import android.app.Application
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.runtime.CompositionLocalProvider
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.HiltAndroidApp
import de.beocode.bestbefore.app.BestBeforeApp
import de.beocode.bestbefore.ui.theme.BestBeforeTheme
import de.beocode.bestbefore.viewmodels.MainViewModel
import de.beocode.bestbefore.viewmodels.interfaces.LocalUserState

@HiltAndroidApp
class App : Application()

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    private val userState by viewModels<MainViewModel>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            BestBeforeTheme {
                CompositionLocalProvider(LocalUserState provides userState) {
                    BestBeforeApp()
                }
            }
        }
    }
}