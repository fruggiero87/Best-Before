package de.beocode.bestbefore.repositories

import androidx.datastore.core.DataStore
import de.beocode.bestbefore.Data
import de.beocode.bestbefore.FoodItem
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map

class DataRepository(private val dataStore: DataStore<Data>) {

    suspend fun setExpireWarning(days: Int) {
        dataStore.updateData { Data ->
            Data.toBuilder()
                .setWarningDays(days)
                .build()
        }
    }

    suspend fun getExpireWarning(): Int {
        val days = dataStore.data.map { Data ->
            Data.warningDays
        }

        return days.first()
    }

    suspend fun addFood(foodItem: FoodItem) {
        dataStore.updateData { Data ->
            Data.toBuilder()
                .addFood(foodItem.toBuilder().setId(System.currentTimeMillis()).build())
                .build()
        }
    }

    suspend fun updateFood(index: Int, foodItem: FoodItem) {
        dataStore.updateData { Data ->
            Data.toBuilder()
                .setFood(index, foodItem)
                .build()
        }
    }

    suspend fun deleteFood(index: Int) {
        dataStore.updateData { Data ->
            Data.toBuilder()
                .removeFood(index)
                .build()
        }
    }

    suspend fun getFoodList(): List<FoodItem> {
        val foodList = dataStore.data.map { Data ->
            Data.foodList
        }

        return foodList.first()
    }

    suspend fun addName(name: String) {
        dataStore.updateData { Data ->
            Data.toBuilder()
                .addName(name)
                .build()
        }
    }

    suspend fun getNameList(): List<String> {
        val nameList = dataStore.data.map { Data ->
            Data.nameList
        }

        return nameList.first()
    }

    suspend fun addDestination(destination: String) {
        dataStore.updateData { Data ->
            Data.toBuilder()
                .addDestination(destination)
                .build()
        }
    }

    suspend fun getDestinationList(): List<String> {
        val destinationList = dataStore.data.map { Data ->
            Data.destinationList
        }

        return destinationList.first()
    }

    suspend fun clearData() {
        dataStore.updateData { Data ->
            Data.toBuilder()
                .clear()
                .setWarningDays(3)
                .build()
        }
    }
}