package de.beocode.bestbefore.screens.general

import android.content.Context
import de.beocode.bestbefore.R
import java.util.*

private const val WEEK = 7
private const val MONTH = 31
private const val YEAR = 365

fun getDiff(ts: Long): Int {
    val cal = Calendar.getInstance(Locale.getDefault())
    cal.timeInMillis = System.currentTimeMillis()
    cal.set(Calendar.HOUR_OF_DAY, 0)
    cal.set(Calendar.MINUTE, 0)

    return ((ts - cal.timeInMillis) / 86400000).toInt()
}

fun convertDiffToText(context: Context, diff: Int): String {
    return if (-1 < diff && diff < 1) {
        context.getString(R.string.expires_today)
    } else if (diff < 0) {
        context.getString(R.string.expired, diffToTimeAgo(context, -diff))
    } else {
        context.getString(R.string.expires_in, diffToTimeAgo(context, diff))
    }
}

private fun diffToTimeAgo(context: Context, diff: Int): String {
    return if (diff < WEEK) {
        context.resources.getQuantityString(R.plurals.day, diff, diff)
    } else if (diff < MONTH) {
        context.resources.getQuantityString(R.plurals.week, diff / WEEK, diff / WEEK)
    } else if (diff < YEAR) {
        context.resources.getQuantityString(R.plurals.month, diff / MONTH, diff / MONTH)
    } else {
        context.resources.getQuantityString(R.plurals.year, diff / YEAR, diff / YEAR)
    }
}