package de.beocode.bestbefore.viewmodels

import android.app.Application
import android.util.Log
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import de.beocode.bestbefore.FoodItem
import de.beocode.bestbefore.data.dataDataStore
import de.beocode.bestbefore.repositories.DataRepository
import de.beocode.bestbefore.viewmodels.interfaces.MainViewModelInterface
import kotlinx.coroutines.launch
import javax.inject.Inject

private const val LOG_TAG = "BBMainViewModel"

@HiltViewModel
class MainViewModel @Inject constructor(application: Application) : MainViewModelInterface,
    ViewModel() {
    private val dataRepository = DataRepository(application.dataDataStore)

    override var isBusy by mutableStateOf(false)

    override var expireWarning by mutableStateOf(0)
    override var foodList by mutableStateOf(emptyList<FoodItem>())
    override var filteredFoodList by mutableStateOf(emptyList<FoodItem>())
    override var nameSuggestions by mutableStateOf(emptyList<String>())
    override var destinationSuggestions by mutableStateOf(emptyList<String>())

    init {
        isBusy = true
        viewModelScope.launch {
            expireWarning = dataRepository.getExpireWarning()
        }
        getFoodList()
    }

    override fun getFoodList() {
        viewModelScope.launch {
            foodList = dataRepository.getFoodList()
            filteredFoodList = foodList.sortedBy { it.dateTS }
            Log.d(LOG_TAG, "Got ${foodList.size} items")
            isBusy = false
        }
    }

    override fun searchFoodList(search: String) {
        filteredFoodList = foodList.filter {
            it.name.contains(search, true)
        }.sortedBy { it.dateTS }
    }

    override fun addFood(foodItem: FoodItem, success: (Boolean) -> Unit) {
        isBusy = true
        viewModelScope.launch {
            try {
                dataRepository.addFood(foodItem)
                Log.d(LOG_TAG, "Added ${foodItem.name}")
                getFoodList()
                success(true)
            } catch (e: Exception) {
                e.printStackTrace()
                success(false)
            }
            getFoodList()
        }
        addSuggestions(foodItem.name, foodItem.destination)
    }

    override fun updateFood(index: Int, foodItem: FoodItem, success: (Boolean) -> Unit) {
        isBusy = true
        viewModelScope.launch {
            try {
                dataRepository.updateFood(index, foodItem)
                Log.d(LOG_TAG, "Updated ${foodItem.name}")
                getFoodList()
                success(true)
            } catch (e: Exception) {
                e.printStackTrace()
                success(false)
            }
            getFoodList()
        }
        addSuggestions(foodItem.name, foodItem.destination)
    }

    override fun deleteFood(foodItem: FoodItem) {
        isBusy = true
        viewModelScope.launch {
            val index = foodList.indexOf(foodItem)
            dataRepository.deleteFood(index)
            Log.d(LOG_TAG, "Deleted ${foodItem.name}")
            getFoodList()
        }
    }

    override fun addSuggestions(name: String, destination: String) {
        viewModelScope.launch {
            if (!nameSuggestions.contains(name))
                dataRepository.addName(name)
            if (!destinationSuggestions.contains(destination) && destination.isNotBlank())
                dataRepository.addDestination(destination)
        }
    }

    override fun getSuggestions() {
        Log.d(LOG_TAG, "Get suggestions...")
        viewModelScope.launch {
            nameSuggestions = dataRepository.getNameList()
            Log.d(LOG_TAG, "Got ${nameSuggestions.size} name suggestions")
        }
        viewModelScope.launch {
            destinationSuggestions = dataRepository.getDestinationList()
            Log.d(LOG_TAG, "Got ${destinationSuggestions.size} destination suggestions")
        }
    }

    override fun setWarningDays(days: Int) {
        viewModelScope.launch {
            dataRepository.setExpireWarning(days)
            expireWarning = dataRepository.getExpireWarning()
            Log.d(LOG_TAG, "Set expire warning to $days days")
        }
    }

    override suspend fun clearData(success: (Boolean) -> Unit) {
        isBusy = true
        try {
            dataRepository.clearData()
            expireWarning = dataRepository.getExpireWarning()
            getFoodList()
            Log.d(LOG_TAG, "Deleted all data")
            success(true)
        } catch (e: Exception) {
            e.printStackTrace()
            success(false)
        }
        isBusy = false
    }
}