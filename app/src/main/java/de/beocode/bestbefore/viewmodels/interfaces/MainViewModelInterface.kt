package de.beocode.bestbefore.viewmodels.interfaces

import androidx.compose.runtime.compositionLocalOf
import de.beocode.bestbefore.FoodItem

interface MainViewModelInterface {

    var isBusy: Boolean

    var expireWarning: Int
    var foodList: List<FoodItem>
    var filteredFoodList: List<FoodItem>
    var nameSuggestions: List<String>
    var destinationSuggestions: List<String>

    fun getFoodList()
    fun searchFoodList(search: String)

    fun addFood(foodItem: FoodItem, success: (Boolean) -> Unit)
    fun updateFood(index: Int, foodItem: FoodItem, success: (Boolean) -> Unit)
    fun deleteFood(foodItem: FoodItem)

    fun addSuggestions(name: String, destination: String)
    fun getSuggestions()

    fun setWarningDays(days: Int)

    suspend fun clearData(success: (Boolean) -> Unit)
}

val LocalUserState = compositionLocalOf<MainViewModelInterface> { error("User State Context not found!") }