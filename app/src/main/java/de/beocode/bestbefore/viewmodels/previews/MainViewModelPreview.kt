package de.beocode.bestbefore.viewmodels.previews

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import de.beocode.bestbefore.FoodItem
import de.beocode.bestbefore.viewmodels.interfaces.MainViewModelInterface

class MainViewModelPreview : MainViewModelInterface, ViewModel() {

    override var isBusy by mutableStateOf(false)

    override var expireWarning by mutableStateOf(0)
    override var foodList by mutableStateOf(emptyList<FoodItem>())
    override var filteredFoodList by mutableStateOf(emptyList<FoodItem>())
    override var nameSuggestions by mutableStateOf(emptyList<String>())
    override var destinationSuggestions by mutableStateOf(emptyList<String>())

    override fun getFoodList() { }
    override fun searchFoodList(search: String) { }

    override fun addFood(foodItem: FoodItem, success: (Boolean) -> Unit) { }
    override fun updateFood(index: Int, foodItem: FoodItem, success: (Boolean) -> Unit) { }
    override fun deleteFood(foodItem: FoodItem) { }

    override fun addSuggestions(name: String, destination: String) { }
    override fun getSuggestions() { }

    override fun setWarningDays(days: Int) { }

    override suspend fun clearData(success: (Boolean) -> Unit) { }
}