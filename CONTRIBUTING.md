# Contributing

## Reporting issues

If you find an issue in the client, you can use our [IssueTracker](https://codeberg.org/BeoCode/Best-Before/issues). Make sure that it hasn't yet been reported by searching first.

Remember to include the following information:

* Android version
* Device model
* App version
* Steps to reproduce the issue

## Translating

You can translate the strings online at [POEditor](https://poeditor.com/join/project/sYYYC34Bgf).
